MERN Stack Task Management Application
This Task Management Application is built using the MERN (MongoDB, Express.js, React.js, Node.js) stack. It allows users to manage tasks with features like creating, updating, deleting, prioritizing, and categorizing.

Clone the repository:
### git clone https://gitlab.com/rustam86310/mern_dev

###    Frontend Development Setup Instructions:  ###

Root directory:
    Install dependencies:
### `npm i`

Runs the app in the development mode.\
### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


###    Backend Development Setup Instructions:    ###

Navigate to the server directory:
### cd server
    Install dependencies:
### `npm i`

Run the server:
### `npm start`

Note: Make sure to install ts-node globally for typescript node server

No need to run mongodb locally.
