import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { TaskCategory, TaskPriority } from '../../services/interface';
import { toast } from 'react-toastify';

const AddTask = () => {
  const initialValues = {
    title: "",
    description: "",
    category: "",
    assignedTo: "",
    priority: ""
  }
  const [inputValues, setInputValues] = useState(initialValues);
  const navigate = useNavigate();
  const location = useLocation();
  const { editData } = location.state || {};

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let result
      if (editData) {
        let axiosObj = {
          method: 'PATCH',
          baseURL: "http://127.0.0.1:3001/",
          url: 'api/task/update_task',
          data: {
            uniqueId: editData.uniqueId,
            title: inputValues.title,
            description: inputValues.description,
            category: inputValues.category,
            assignedTo: inputValues.assignedTo,
            priority: inputValues.priority
          }
        }
        result = await axios(axiosObj);
      } else {
        let axiosObj = {
          method: 'POST',
          baseURL: "http://127.0.0.1:3001/",
          url: 'api/task/create_task',
          data: inputValues,
        }
        result = await axios(axiosObj);
      }
      if (result?.data?.success) {
        toast.success(`Successfully ${editData ? 'Updated' : 'Added'}!`, { toastId: "add-task" });
        navigate('/');
      } else {
        toast.error(result.data.result.error, { toastId: "add-task" });
      }
    } catch (e) {
      console.log(e);
      toast.error(e.message, { toastId: "add-task" });
    }
  }

  const handleChange = (e) => {
    const { name, value } = e.target
    setInputValues({
      ...inputValues,
      [name]: value
    })
  }

  useEffect(() => {
    if (editData) {
      setInputValues(editData);
    }
  }, [])


  return (
    <div className="container main-section">
      <div className="form-box">
        <h1 className='text-center'>Add Task</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-group py-1">
            <label for="title">Title</label>
            <input
              className="form-control"
              name='title'
              onChange={handleChange}
              type="text"
              value={inputValues.title}
            />
          </div>
          <div className="form-group py-1">
            <label for="email">Description</label>
            <input
              className="form-control"
              name='description'
              type="text"
              onChange={handleChange}
              value={inputValues.description}
            />
          </div>
          <div className="form-group py-1">
            <label for="email">Assigned To</label>
            <input
              className="form-control"
              name='assignedTo'
              type="text"
              onChange={handleChange}
              value={inputValues.assignedTo}
            />
          </div>
          <div className="form-group py-1">
            <label for="email">Category</label>
            <select
              name='category'
              onChange={handleChange}
              value={inputValues.category}
              className="form-select"
            >
              <option value="" disabled>Select Category</option>
              {
                Object.values(TaskCategory).map(cate => (
                  <option value={cate}>{cate}</option>
                ))
              }
            </select>
          </div>
          {/* {
            editData && ( */}
          <div className="form-group py-1">
            <label for="email">Priority</label>
            <select
              name='priority'
              onChange={handleChange}
              value={inputValues.priority}
              className="form-select"
            >
              <option value="" disabled>Select Priority</option>
              {
                Object.values(TaskPriority).map(cate => (
                  <option value={cate}>{cate}</option>
                ))
              }
            </select>
          </div>
          {/* )
          } */}
          <div className="form-group py-3 d-flex justify-content-end">
            <Link to={'/'} className="btn btn-danger mx-3">cancel</Link>
            <button className="btn btn-primary" type="submit">Submit</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddTask;