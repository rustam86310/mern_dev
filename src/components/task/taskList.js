import axios from 'axios';
import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';
import { TaskCategory, TaskPriority, TaskStatus } from '../../services/interface';
import { toast } from 'react-toastify';

const TaskList = () => {
    const navigate = useNavigate();
    const [taskData, setTaskData] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [category, setCategory] = useState("");
    const [deleteId, setDeleteId] = useState("");

    const handleDelete = async () => {
        try {
            let axiosObj = {
                method: 'DELETE',
                baseURL: "http://127.0.0.1:3001/",
                url: 'api/task/delete_task',
                params: { uniqueId: deleteId }
            }
            let result = await axios(axiosObj);
            if (result?.data?.success) {
                toast.success("Successfully Deleted.", { toastId: "delete-task" });
                let data = taskData.filter(d => d.uniqueId !== result.data.result.message.uniqueId);
                setTaskData(data);
            } else {
                toast.error(result.data.result.error, { toastId: "delete-task" });
            }
        } catch (e) {
            toast.error(e.message, { toastId: "delete-task" });
        }
    }

    const columns = [
        {
            name: 'Id',
            selector: (row, i) => i + 1,
            sortable: true
        },
        {
            name: 'Title',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Description',
            selector: row => row.description,
            sortable: true
        },
        {
            name: 'Category',
            selector: row => row.category,
            sortable: true
        },
        {
            name: 'Assigned To',
            selector: row => row.assignedTo,
            sortable: true
        },
        {
            name: 'Status',
            selector: row => <span className={`badge bg-${row.status === TaskStatus.CANCELLED ? 'danger' : row.status === TaskStatus.COMPLETED ? 'success' : 'primary'}`}>{row.status}</span>,
        },
        {
            name: 'Priority',
            selector: row => row.priority,
            sortable: true
        },
        {
            name: 'Date',
            width: "200px",
            selector: row => row.createdAt,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => (
                <div>
                    <Link to={'/add-task'} title='Edit' state={{ editData: row }}><i className='fa fa-edit fa-2x' /></Link>
                    <span title='Delete' onClick={() => setDeleteId(row.uniqueId)} data-bs-toggle="modal" data-bs-target="#staticBackdrop" className='mx-2' type="button"><i className='fa fa-trash fa-2x text-danger' /></span>
                </div>
            ),
        },
    ];


    const handleAddTask = () => {
        navigate('/add-task');
    }

    const getTaskData = async () => {
        try {
            let params = {
                searchTerm,
                category,
                page: -1
            }
            let axiosObj = {
                method: 'GET',
                baseURL: "http://127.0.0.1:3001/",
                url: 'api/task/get_task_list',
                params: params
            }
            let result = await axios(axiosObj);
            if (result?.data?.success) {
                setTaskData(result.data.result.message.data)
            } else {
                toast.error(result.data.result.error, { toastId: "task-list" });
            }
        } catch (e) {
            console.log(e);
            toast.error(e.message, { toastId: "task-list" });
        }
    }

    const resetData = () => {
        setSearchTerm('');
        setCategory('');
    }

    useEffect(() => {
        getTaskData();
        return () => {
            setTaskData([]);
        }
    }, [searchTerm, category])

    return (
        <div className="container main-section">
            <div className='d-md-flex justify-content-between py-3'>
                <div className='d-md-flex align-items-center'>
                    <select className="form-select" value={category} onChange={(e) => setCategory(e.target.value)}>
                        <option value="" disabled>Select Category</option>
                        {Object.values(TaskCategory).map(cate => (
                            <option value={cate}>{cate}</option>
                        ))}
                    </select>
                    <input className="form-control mx-md-3" placeholder='Search' onChange={(e) => setSearchTerm(e.target.value)} value={searchTerm} type="text" />
                    <button title='Reset' className='btn btn-info text-light' onClick={resetData}><i className='fa fa-sync' /></button>
                </div>
                <div>
                    <button className="btn btn-success" onClick={handleAddTask}><i className='fa fa-plus' /> Add Task</button>
                </div>
            </div>
            <DataTable
                columns={columns}
                data={taskData}
                pagination
                keyField="uniqueId"
                responsive
                paginationPerPage={10}
                paginationRowsPerPageOptions={[10, 20]}
                highlightOnHover
                fixedHeader
                fixedHeaderScrollHeight="calc(100vh - 250px)"
            />
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Delete Confirmation</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            Wohoo!! Are you going to delete it???
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-danger" data-bs-dismiss="modal" onClick={handleDelete}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TaskList;