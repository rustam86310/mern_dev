export const TaskCategory = {
    PERSONAL: "Personal",
    HEALTH: "Health",
    EDUCATION: "Education",
    SOCIAL: "Social",
    FINANCE: "Finance"
}
export const TaskPriority = {
    HIGH: "High",
    MEDIUM: "Medium",
    LOW: "Low",
}
export const TaskStatus = {
    COMPLETED: "Completed",
    PENDING: "Pending",
    CANCELLED: "Canceled",
}