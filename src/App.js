import { BrowserRouter, Route, Routes } from "react-router-dom";
import TaskList from "./components/task/taskList";
import AddTask from "./components/task/addTask";
import Header from "./components/common/header";
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" Component={TaskList} />
          <Route path="/add-task" Component={AddTask} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
