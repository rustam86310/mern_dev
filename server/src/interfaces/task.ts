export interface TaskStruct {
    uniqueId: string;
    title: string;
    description: string;
    priority: string;
    category: string;
    status: string;
    isDeleted: boolean;
    _id?: string;
}
export enum TaskCategory {
    PERSONAL = "Personal",
    HEALTH = "Health",
    EDUCATION = "Education",
    SOCIAL = "Social",
    FINANCE = "Finance"
}
export enum TaskPriority {
    HIGH = "High",
    MEDIUM = "Medium",
    LOW = "Low",
}
export enum TaskStatus {
    COMPLETED = "Completed",
    PENDING = "Pending",
    CANCELLED = "Canceled",
}