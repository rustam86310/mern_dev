import { TaskPriority, TaskStatus, TaskStruct } from "@/interfaces/task";
import { Request, Response } from "express";
import { Inject, Service } from "typedi";

@Service()
export default class TaskService {

    constructor(
        @Inject('logger') private logger,
        @Inject('taskModel') private taskModel: Models.TaskModel
    ) { }

    public async createTask(req: Request, res: Response)
        : Promise<{ message: string | TaskStruct, flag: boolean }> {
        try {
            let obj: TaskStruct = {
                uniqueId: process.env.TASK_PREFIX + (new Date()).getTime(),
                status: TaskStatus.PENDING,
                isDeleted: false,
                ...req.body
            };

            let entry: TaskStruct = await this.taskModel.create(obj) as TaskStruct;
            return { message: entry, flag: true };
        } catch (e) {
            this.logger.error(e.message);
            return { message: e.message, flag: false }
        }
    }

    public async updateTask(req: Request, res: Response)
        : Promise<{ message: string | TaskStruct, flag: boolean }> {
        try {
            let { uniqueId, ...obj } = req.body;

            let entry: TaskStruct = await this.taskModel.findOneAndUpdate({ uniqueId: uniqueId },
                obj, { new: true, upsert: true }) as TaskStruct;
            return { message: entry, flag: true };
        } catch (e) {
            this.logger.error(e.message);
            return { message: e.message, flag: false }
        }
    }

    public async getTaskList(req: Request, res: Response)
        : Promise<{ message: string | { data: TaskStruct[], totalCount: number }, flag: boolean }> {
        try {
            let { searchTerm, category } = req.query;
            let aggregate = [];
            let page = +req.query.page;
            let obj = {
                $facet: {
                    totalCount: [
                        {
                            $count: "count"
                        },
                        {
                            $project: { count: 1, _id: 0 }
                        }
                    ],
                    data: []
                }
            };
            let matchObj = {
                $match: {
                    $or: [],
                    isDeleted: false
                }
            };

            if (searchTerm) {
                let searchTerm = req.query?.searchTerm.toString();
                matchObj.$match.$or.push({ title: { $regex: searchTerm, $options: "i" } });
                matchObj.$match.$or.push({ description: { $regex: searchTerm, $options: "i" } });
                matchObj.$match.$or.push({ assignedTo: { $regex: searchTerm, $options: "i" } });
            } else {
                delete matchObj.$match.$or
            }

            if (category) {
                Object.assign(matchObj.$match, { category: category });
            }

            obj.$facet.data.push(matchObj);
            // @ts-ignore
            obj.$facet.totalCount.splice(0, 0, matchObj);

            // Switching string to number for sorting
            obj.$facet.data.push({
                $addFields: {
                    priorityValue: {
                        $switch: {
                            branches: [
                                { case: { $eq: ["$priority", TaskPriority.HIGH] }, then: 3 },
                                { case: { $eq: ["$priority", TaskPriority.MEDIUM] }, then: 2 },
                                { case: { $eq: ["$priority", TaskPriority.LOW] }, then: 1 }
                            ],
                            default: 0
                        }
                    }
                }
            })

            obj.$facet.data.push({ $sort: { priorityValue: -1, createdAt: -1 } });
            if (page !== -1) {
                let startIndex = (page - 1) * +process.env.LIMIT;
                obj.$facet.data.push({ $skip: startIndex })
                obj.$facet.data.push({ $limit: +process.env.LIMIT })
            }
            aggregate.push(obj);
            let query = await this.taskModel.aggregate(aggregate).allowDiskUse(true);
            if (query.length === 0) {
                return { message: { data: [], totalCount: 0 }, flag: true };
            }
            let result = query[0];

            return {
                message: {
                    data: result["data"],
                    totalCount: result["totalCount"].length === 0 ? 0 : result["totalCount"][0].count
                }, flag: true
            };
        } catch (e) {
            this.logger.error(e.message);
            return { message: e.message, flag: false }
        }
    }

    public async deleteTask(req: Request, res: Response)
        : Promise<{ message: string | TaskStruct, flag: boolean }> {
        try {
            let entry: TaskStruct = await this.taskModel.findOneAndUpdate({ uniqueId: req.query.uniqueId.toString() },
                { isDeleted: true }, { new: true }) as TaskStruct;
            return { message: entry, flag: true };
        } catch (e) {
            this.logger.error(e.message);
            return { message: e.message, flag: false }
        }
    }
}