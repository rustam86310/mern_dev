import { Request, Response, Router } from "express";
import { celebrate, Joi } from "celebrate";
import { TaskCategory, TaskPriority, TaskStatus, TaskStruct } from "@/interfaces/task";
import TaskService from "@/services/taskServices";
import { Container } from "typedi/Container";

const route = Router();

export default (app: Router) => {

    app.use("/task", route);

    route.post("/create_task", celebrate({
        body: Joi.object().keys({
            title: Joi.string().required(),
            description: Joi.string().required(),
            assignedTo: Joi.string().required(),
            priority: Joi.string().required().valid(TaskPriority.HIGH).valid(TaskPriority.LOW).valid(TaskPriority.MEDIUM),
            category: Joi.string().required().valid(TaskCategory.EDUCATION).valid(TaskCategory.FINANCE).valid(TaskCategory.HEALTH).valid(TaskCategory.PERSONAL).valid(TaskCategory.SOCIAL),
        })
    }),
        async (req: Request, res: Response) => {
            const result: { message: string | TaskStruct, flag: boolean } = await Container.get(TaskService)
                .createTask(req, res);
            if (result.flag) {
                return res.status(200).json({ success: true, result: { message: result.message } });
            } else {
                return res.status(200).json({ success: false, result: { error: result.message } });
            }
        }
    );

    route.patch("/update_task", celebrate({
        body: Joi.object().keys({
            uniqueId: Joi.string().required(),
            title: Joi.string(),
            description: Joi.string(),
            assignedTo: Joi.string(),
            priority: Joi.string().valid(TaskPriority.HIGH).valid(TaskPriority.LOW).valid(TaskPriority.MEDIUM),
            category: Joi.string().valid(TaskCategory.EDUCATION).valid(TaskCategory.FINANCE).valid(TaskCategory.HEALTH).valid(TaskCategory.PERSONAL).valid(TaskCategory.SOCIAL),
            status: Joi.string().valid(TaskStatus.CANCELLED).valid(TaskStatus.COMPLETED).valid(TaskStatus.PENDING)
        })
    }),
        async (req: Request, res: Response) => {
            const result: { message: string | TaskStruct, flag: boolean } = await Container.get(TaskService)
                .updateTask(req, res);
            if (result.flag) {
                return res.status(200).json({ success: true, result: { message: result.message } });
            } else {
                return res.status(200).json({ success: false, result: { error: result.message } });
            }
        }
    );

    route.get("/get_task_list", celebrate({
        query: Joi.object().keys({
            page: Joi.string().required(),
            searchTerm: Joi.string().allow("").allow(null),
            category: Joi.string().allow("").allow(null).valid(TaskCategory.EDUCATION).valid(TaskCategory.FINANCE).valid(TaskCategory.HEALTH).valid(TaskCategory.PERSONAL).valid(TaskCategory.SOCIAL),
        })
    }),
        async (req: Request, res: Response) => {
            const result: { message: string | { data: TaskStruct[], totalCount: number }, flag: boolean } = await Container.get(TaskService)
                .getTaskList(req, res);
            if (result.flag) {
                return res.status(200).json({ success: true, result: { message: result.message } });
            } else {
                return res.status(200).json({ success: false, result: { error: result.message } });
            }
        }
    );

    route.delete("/delete_task", celebrate({
        query: Joi.object().keys({
            uniqueId: Joi.string().required()
        })
    }),
        async (req: Request, res: Response) => {
            const result: { message: string | TaskStruct, flag: boolean } = await Container.get(TaskService)
                .deleteTask(req, res);
            if (result.flag) {
                return res.status(200).json({ success: true, result: { message: result.message } });
            } else {
                return res.status(200).json({ success: false, result: { error: result.message } });
            }
        }
    );

}
