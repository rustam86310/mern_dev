import { Router } from 'express';
import task_route from './routes/task_route';

// guaranteed to get dependencies
export default () => {
	const app = Router();
	task_route(app);

	return app
}