import { TaskCategory, TaskPriority, TaskStatus, TaskStruct } from '@/interfaces/task';
import mongoose from 'mongoose';

const Task = new mongoose.Schema(
  {
    uniqueId: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
    priority: { type: String, required: true, enum: [TaskPriority.HIGH, TaskPriority.LOW, TaskPriority.MEDIUM] },
    category: { type: String, required: true },
    status: { type: String, default: TaskStatus.PENDING, enum: [TaskStatus.PENDING, TaskStatus.COMPLETED, TaskStatus.CANCELLED]},
    isDeleted: { type: Boolean, default: true },
    assignedTo: { type: String, default: "" }
  },
  { timestamps: true },
);

export default mongoose.model<TaskStruct & mongoose.Document>('Task', Task);
